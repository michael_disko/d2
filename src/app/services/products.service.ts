import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';

import { baseUrl } from '../configuration/base-url';
import { Product } from '../interfaces/product';

@Injectable()
export class ProductsService {
  url: string;
  private products: Product[];

  constructor(private httpClient: HttpClient, private toastr: ToastrService) {
    this.url = baseUrl;
    this.products = [];
  }

  get allProducts() {
    return this.products;
  }

  set newDataProducts(data) {
    this.products = data;
  }

  getProducts(): Observable<any> {
    return this.httpClient.get(`${baseUrl}/products`).delay(0);
  }

  addProduct(product: Product) {
    const presence = this.products.some(el => {
      return el.id === product.id;
    });
    if (!presence) {
      this.products.push({
        id: product.id,
        image: product.image,
        ingredients: product.ingredients,
        name: product.name,
        price: product.price,
        selected: true
      });
      this.toastr.info('Product added to cart');
    } else {
      this.toastr.warning('Product has already been added to the cart');
    }
  }

  removeProduct(id: number) {
    this.products = this.products.filter(item => item.id !== id);
    this.toastr.success('The product is removed from the cart');
  }
}
