import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Libraries
import { ToastrModule } from 'ngx-toastr';

// Modules
import { AppRoutingModule } from './routing/app-routing.module';

// Services
import { ProductsService } from './services/products.service';
import { HistoryService } from './services/history.service';

// Pipes
import { FilterPipe } from './pipes/filter.pipe';
import { SortingPipe } from './pipes/sorting.pipe';

// Components
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { HistoryComponent } from './components/history/history.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CartComponent } from './components/cart/cart.component';
import { ThumbComponent } from './components/thumb/thumb.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ItemComponent } from './components/history/item/item.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    HistoryComponent,
    NotFoundComponent,
    CartComponent,
    ThumbComponent,
    FilterPipe,
    SortingPipe,
    LoaderComponent,
    ItemComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [ProductsService, HistoryService],
  bootstrap: [AppComponent]
})
export class AppModule {}
