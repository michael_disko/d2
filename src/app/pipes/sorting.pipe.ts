import { Pipe, PipeTransform } from '@angular/core';

import { Product } from '../interfaces/product';

@Pipe({
  name: 'sorting'
})
export class SortingPipe implements PipeTransform {
  transform(array: Product[], value: string, type: string): Product[] {
    array.sort((a: Product, b: Product) => {
      if (a[type] < b[type]) {
        return value === 'low' ? -1 : 1;
      } else if (a[type] > b[type]) {
        return value === 'low' ? 1 : -1;
      } else {
        return 0;
      }
    });
    return array;
  }
}
