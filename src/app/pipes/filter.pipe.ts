import { Pipe, PipeTransform } from '@angular/core';

import { Product } from '../interfaces/product';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(array: Product[], query: string): Product[] {
    if (array === null || query === '') {
      return array;
    }

    return array.filter(value => {
      return value.name.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    });
  }
}
