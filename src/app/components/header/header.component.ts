import { Component, OnInit } from '@angular/core';

import { fadeInAnimation } from '../../animations/index';
import { ProductsService } from './../../services/products.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [fadeInAnimation]
})
export class HeaderComponent implements OnInit {
  constructor(public productsService: ProductsService) {}

  ngOnInit() {}
}
