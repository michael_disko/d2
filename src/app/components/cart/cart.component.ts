import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { fadeInAnimation } from '../../animations/index';
import { Product } from '../../interfaces/product';
import { History } from '../../interfaces/history';
import { ProductsService } from '../../services/products.service';
import { HistoryService } from '../../services/history.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  animations: [fadeInAnimation]
})
export class CartComponent implements OnInit, AfterContentChecked {
  total: number;

  constructor(
    public productsService: ProductsService,
    private historyService: HistoryService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.total = 0;
  }

  ngOnInit() {}

  removeFromToCart(product: Product) {
    console.log(product);
    this.productsService.removeProduct(product.id);
  }

  ngAfterContentChecked() {
    this.total = this.productsService.allProducts.reduce(
      (previousValue, currentValue) => {
        return previousValue + currentValue.price;
      },
      0
    );
  }

  setHistory() {
    const history = {
      date: new Date().getTime(),
      product: this.productsService.allProducts,
      amount: this.total
    };
    this.historyService.setHistory(history).subscribe(
      res => {
        this.toastr.success('You have successfully made a purchase');
        this.productsService.newDataProducts = [];
        this.router.navigate(['/history']);
      },
      error => this.toastr.error(error.message)
    );
  }
}
