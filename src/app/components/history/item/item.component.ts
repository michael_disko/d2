import { Component, OnInit, Input } from '@angular/core';

import { History } from '../../../interfaces/history';
import { fadeInAnimation } from '../../../animations/index';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
  animations: [fadeInAnimation]
})
export class ItemComponent implements OnInit {
  @Input() history: History;

  constructor() {}

  ngOnInit() {
  }
}
