import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { fadeInAnimation } from '../../animations/index';
import { History } from '../../interfaces/history';
import { HistoryService } from '../../services/history.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  animations: [fadeInAnimation]
})
export class HistoryComponent implements OnInit {
  loading: boolean;
  sortingValue: string;
  sortingType: string;
  histories: History[];

  constructor(
    private historyService: HistoryService,
    private toastr: ToastrService
  ) {
    this.loading = false;
  }

  ngOnInit() {
    this.historyService.getHistory().subscribe(
      (data: History[]) => {
        if (data.length) {
          this.histories = data;
          this.loading = true;
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error(error.message);
        this.loading = true;
      }
    );
  }

  sortingProducts(event) {
    const _this = event.currentTarget;
    const value = _this.getAttribute('data-sorting-value');
    const type = _this.getAttribute('data-sorting-type');
    this.sortingValue = value;
    this.sortingType = type;
  }
}
