import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';

import { baseUrl } from '../configuration/base-url';
import { History } from '../interfaces/history';

@Injectable()
export class HistoryService {
  url: string;

  constructor(private httpClient: HttpClient, private toastr: ToastrService) {
    this.url = baseUrl;
  }

  getHistory(): Observable<any> {
    return this.httpClient.get(`${baseUrl}/histories`).delay(0);
  }

  setHistory(item: History) {
    return this.httpClient.post(`${baseUrl}/histories`, item).delay(0);
  }
}
