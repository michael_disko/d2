import { Product } from './product';
export interface History {
  id?: number;
  date: number;
  product: Product[];
  amount: number;
}
