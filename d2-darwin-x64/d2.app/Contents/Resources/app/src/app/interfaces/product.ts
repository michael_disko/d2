export interface Product {
  id?: number;
  name: string;
  ingredients: [string];
  price: number;
  selected: boolean;
  image?: string;
}
