import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { fadeInAnimation } from '../../animations/index';
import { Product } from './../../interfaces/product';

@Component({
  selector: 'app-thumb',
  templateUrl: './thumb.component.html',
  styleUrls: ['./thumb.component.scss'],
  animations: [fadeInAnimation]
})
export class ThumbComponent implements OnInit {
  @Input() product: Product;
  @Output() addToCart = new EventEmitter<Product>();
  @Output() removeFromToCart = new EventEmitter<Product>();
  constructor() {}

  ngOnInit() {}

  chooseProduct(product: Product) {
    this.addToCart.emit(product);
  }

  removeProduct(product: Product) {
    this.removeFromToCart.emit(product);
  }
}
