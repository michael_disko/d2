import { Component, OnInit } from '@angular/core';

import { fadeInAnimation } from '../../animations/index';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
  animations: [fadeInAnimation]
})
export class NotFoundComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
