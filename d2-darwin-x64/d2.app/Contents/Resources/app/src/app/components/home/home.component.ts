import { Component, OnInit, HostBinding } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

import { ProductsService } from '../../services/products.service';
import { Product } from '../../interfaces/product';
import { fadeInAnimation } from '../../animations/index';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [fadeInAnimation]
})
export class HomeComponent implements OnInit {
  products: Product[];
  query: string;
  sortingValue: string;
  sortingType: string;
  loading: boolean;

  constructor(
    private productsService: ProductsService,
    private toastr: ToastrService
  ) {
    this.query = '';
    this.sortingValue = '';
    this.sortingType = '';
    this.loading = false;
  }

  ngOnInit() {
    this.productsService.getProducts().subscribe(
      (data: Product[]) => {
        if (data.length) {
          this.products = data;
          this.loading = true;
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error(error.message);
        this.loading = true;
      }
    );
  }

  sortingProducts(event) {
    const _this = event.currentTarget;
    const value = _this.getAttribute('data-sorting-value');
    const type = _this.getAttribute('data-sorting-type');
    this.sortingValue = value;
    this.sortingType = type;
  }

  addToCart(product: Product) {
    this.productsService.addProduct(product);
  }
}
