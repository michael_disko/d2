import { Component, OnInit } from '@angular/core';

import { fadeInAnimation } from '../../animations/index';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
  animations: [fadeInAnimation]
})
export class LoaderComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
